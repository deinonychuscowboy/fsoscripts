#!/usr/bin/env python3
import os,re

src_folders=[
    "/path/to/fso/data/tables/",
    "/path/to/submodule/data/tables/",
]
dest_folder= os.path.dirname(__file__)+"/out/"
table_filename="ships.tbl"
modular_suffix="-shp.tbm"

entries={}

commentr=re.compile(r";.*")

class KeyInstance:
    def __init__(self,name):
        self.__name=name
        self.__next=None
        self.__prev=None
    def link_next(self,other):
        self.__next=other
    def link_prev(self,other):
        self.__prev=other
    @property
    def name(self):
        return self.__name
    @property
    def next(self):
        return self.__next
    @property
    def prev(self):
        return self.__prev
    def __str__(self):
        return self.__name
    def __repr__(self):
        return self.__name

class KeyCollection:
    keys={}
    def __init__(self,section):
        self.__pointer=None
        self.__flag_last=True
        self.__section=section
        self.__dict={}
        if section not in self.keys:
            self.keys[section]=[]
        if len(self.keys[self.__section])!=0:
            self.__pointer=self.keys[self.__section][0]
            self.__flag_last=False
    def add(self,key,value):
        if self.__pointer is None:
            if len(self.keys[self.__section])==0:
                self.keys[self.__section].append(KeyInstance(key))
                self.__dict[self.keys[self.__section][0]]=value
                return
            self.__pointer=self.keys[self.__section][-1]
            self.__flag_last=True
        p=self.__pointer
        while p is not None:
            if p.name==key:
                self.__pointer=p
                break
            p=p.next
        if self.__pointer.name!=key:
            if self.__flag_last:
                prev=self.__pointer
                next=self.__pointer.next
            else:
                prev=self.__pointer.prev
                next=self.__pointer
            new=KeyInstance(key)
            if prev is not None:
                prev.link_next(new)
                new.link_prev(prev)
            if next is not None:
                next.link_prev(new)
                new.link_next(next)
            self.keys[self.__section].append(new)
            self.__pointer=new
        self.__dict[self.__pointer]=value
        self.__pointer=self.__pointer.next
    def get(self,key):
        pointer=self.keys[self.__section][0]
        s=[]
        while pointer is not None:
            if pointer.name==key:
                s.append(self.__dict[pointer])
            pointer=pointer.next
        return s
    def output(self):
        pointer=self.keys[self.__section][0]
        s=[]
        while pointer is not None:
            s.append([pointer.name,self.__dict[pointer] if pointer in self.__dict else None])
            pointer=pointer.next
        return s

def stripcomments(string):
    return commentr.sub("",string)

def load(path):
    section=""
    contents=[]
    contents2=[]
    with open(path,"r") as f:
        for line in f:
            contents.append(line.strip())
    for row in range(0,len(contents)):
        if contents[row].strip()=="$end_multi_text":
            val=""
            for row2 in range(row-1,-1,-1):
                val=contents2.pop()+val
                if (val[0]=="+" or val[0]=="$") and ":" in val:
                    break
            contents2.append(val)
        else:
            contents2.append(contents[row])
    for line in contents2:
        line=stripcomments(line).strip()
        if len(line)>0:
            if line.startswith("#"):
                section=line[1:]
            elif section != "End":
                key=line.split(":")[0].strip()
                value=":".join(line.split(":")[1:]).strip()
                sigil=key[0]
                key=key[1:].lower().title()

                if key=="Name":
                    if section not in entries:
                        entries[section]=[]
                    entries[section].append(KeyCollection(section))
                entries[section][-1].add(key,value)

def run():
    for folder in src_folders:
        for path,_,files in os.walk(folder):
            for file in files:
                if file.endswith(modular_suffix) or file==table_filename:
                    load(path+file)
    for section in entries:
        section_file=[]
        for collection in entries[section]:
            section_file.append(collection.output())
        pass
        os.makedirs(dest_folder,exist_ok=True)
        pivot=[]
        with open(dest_folder+section+".csv","w") as f:
            for record in section_file[0]:
                pivot.append(record[0]+",")
            for entry in section_file:
                index=0
                for record in entry:
                    pivot[index]+="\""+(record[1].replace("\"","") if record[1] is not None else "")+"\","
                    index+=1
            for line in pivot:
                f.write(line+"\n")

if __name__ == "__main__":
    run()
