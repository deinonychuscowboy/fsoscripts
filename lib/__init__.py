def get_hue(color):
	color=color.replace("#","")
	r=int(color[:2],base=16)/255.
	g=int(color[2:4],base=16)/255.
	b=int(color[4:],base=16)/255.
	cmax=max(r,g,b)
	cmin=min(r,g,b)
	delta=cmax-cmin
	if delta==0:
		return 0
	if cmax==r:
		return 60*(((g-b)/delta)%6)
	elif cmax==g:
		return 60*(((b-r)/delta)+2)
	else:
		return 60*(((r-g)/delta)+4)

def get_rgb(hue,saturation=1.0,value=1.0):
	c=saturation*value
	h=hue/60
	x=c*(1-abs(h%2-1))
	r1,g1,b1= ((c,x,0) if 0<=h<1 else
	           (x,c,0) if 1<=h<2 else
			   (0,c,x) if 2<=h<3 else
			   (0,x,c) if 3<=h<4 else
			   (x,0,c) if 4<=h<5 else
			   (c,0,x))
	m=value-c
	return int((r1+m)*255),int((g1+m)*255),int((b1+m)*255)
