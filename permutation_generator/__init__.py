#!/usr/bin/env python3
import os,ruamel.yaml
from lib import *
yaml = ruamel.yaml.YAML(typ='rt')

files={}
colors={
  "red":355,
  "orange":35,
  "yellow":55,
  "chartreuse":90,
  "green":120,
  "seafoam":150,
  "cyan":180,
  "azure":205,
  "blue":240,
  "indigo":270,
  "purple":290,
  "magenta":320,
}

sizes=[
	"lght",
	"std",
	"hvy",
	"super",
	"ultra",
	"mega",
]

speeds=[
	"fast",
	"mod",
	"slow"
]


def generate(params,color,size,speed,filename,content):
	if params["name"][:4]=="beam":
		if color=="purple" and size in ["lght","hvy","ultra"]: # skip half of beams for mandate
			return
		if color=="cyan" and size in ["super","ultra","mega"]: # union only has small beams
			return
		if color=="chartreuse" and size in ["hvy","super","ultra","mega"]: # colonials only have very small beams
			return
		if color in ["yellow","cyan","chartreuse"] and speed=="slow": # confed, union, colonials can't fire a long time
			return
		if size in ["ultra","mega"] and speed in ["fast","slow"]: # big beams only have one duration
			return
	global files
	computed = {}
	for name in params.keys():
		if name.startswith("base_"):
			computed[name.replace("base_", "")]=round(float(params[name]) * params[name.replace("base_", "mult_")] ** (0 if size == "lght" else 1 if size == "std" else 1.9 if size == "hvy" else 2.7 if size=="super" else 3.3 if size=="ultra" else 3.8),2)
			computed[name.replace("base_","")+"_int"]=int(round(computed[name.replace("base_", "")]))
	computed["color"] = color.title()
	computed["colorkey"] = color[0] if len(color)>0 and color != "chartreuse" else "h"
	computed["colorrgb"] = ",".join(str(x) for x in get_rgb(colors[color]))
	computed["name"] = params["name"].title()
	computed["size"] = size.title()
	computed["type"] = params["type"].title()
	computed["speed"] = speed.title()
	computed["speed_value"] = 2.5 if speed=="fast" else 5.0 if speed=="mod" else 7.5 if speed=="slow" else 0
	computed["beamsnd"]=149 if size=="lght" else 146 if size=="std" else 147 if size=="hvy" else 148 if size=="super" else 144 if size=="ultra" else 145
	computed["warmup"]=1000 if size=="lght" else 1500 if size=="std" else 2500 if size=="hvy" else 3000 if size=="super" else 3500 if size=="ultra" else 5000 # these need to match up exactly with the warmupsnds below
	computed["warmupsnd"]=150 if size=="lght" else 151 if size=="std" else 152 if size=="hvy" else 153 if size=="super" else 154 if size=="ultra" else 155
	computed["warmdownsnd"]=161 if size=="lght" else 158 if size=="std" else 159 if size=="hvy" else 160 if size=="super" else 156 if size=="ultra" else 157 # these need to match up exactly with the beamsnds above

	flicker_mult=1.0
	if color in ["yellow","chartreuse"]:
		flicker_mult=1.0
		if size in ["lght","std"]:
			computed["glowkey"]=""
			computed["section0_type"]="hazy"
			computed["section1_type"]="diffuse"
			computed["section2_type"]="plasma"
			computed["section3_type"]="sstab-haze"
			computed["section4_type"]="sstab-glow"
		elif size in ["hvy","super"]:
			computed["glowkey"]=9
			computed["section0_type"]="sstab"
			computed["section1_type"]="sstab-haze"
			computed["section2_type"]="sstab-glow"
			computed["section3_type"]="sstab-ghaze"
			computed["section4_type"]="whoosh-glow"
		elif size in ["ultra","mega"]:
			computed["glowkey"]=8
			computed["section0_type"]="ustab"
			computed["section1_type"]="sstab-haze"
			computed["section2_type"]="whoosh-glow"
			computed["section3_type"]="whoosh-ghaze"
			computed["section4_type"]="spor"
	if color in ["green","cyan"]:
		flicker_mult=0.67
		if size in ["lght","std"]:
			computed["glowkey"]=2
			computed["section0_type"]="whoosh"
			computed["section1_type"]="diffuse"
			computed["section2_type"]="plasma"
			computed["section3_type"]="sstab-haze"
			computed["section4_type"]="sstab-glow"
		elif size in ["hvy","super"]:
			computed["glowkey"]=5
			computed["section0_type"]="cloud"
			computed["section1_type"]="sstab-haze"
			computed["section2_type"]="sstab-glow"
			computed["section3_type"]="sstab-ghaze"
			computed["section4_type"]="whoosh-glow"
		elif size in ["ultra","mega"]:
			computed["glowkey"]=4
			computed["section0_type"]="spiky"
			computed["section1_type"]="sstab-haze"
			computed["section2_type"]="whoosh-glow"
			computed["section3_type"]="whoosh-ghaze"
			computed["section4_type"]="sstab-ghaze"
	if color=="blue":
		flicker_mult=0.33
		if size in ["lght","std"]:
			computed["glowkey"]=6
			computed["section0_type"]="stable"
			computed["section1_type"]="stream"
			computed["section2_type"]="plasma"
			computed["section3_type"]="sstab-haze"
			computed["section4_type"]="sstab-glow"
		elif size in ["hvy","super"]:
			computed["glowkey"]=3
			computed["section0_type"]="stable"
			computed["section1_type"]="stream"
			computed["section2_type"]="sstab-haze"
			computed["section3_type"]="sstab-glow"
			computed["section4_type"]="sstab-ghaze"
		elif size in ["ultra","mega"]:
			computed["glowkey"]=7
			computed["section0_type"]="stable"
			computed["section1_type"]="stream"
			computed["section2_type"]="diffuse"
			computed["section3_type"]="whoosh-glow"
			computed["section4_type"]="whoosh-ghaze"
	if color=="purple":
		flicker_mult=0.01
		if size in ["lght","std"]:
			computed["glowkey"]=6
			computed["section0_type"]="elec"
			computed["section1_type"]="psy"
			computed["section2_type"]="spor"
			computed["section3_type"]="diffuse"
			computed["section4_type"]="stream"
		elif size in ["hvy","super"]:
			computed["glowkey"]=3
			computed["section0_type"]="elec"
			computed["section1_type"]="psy"
			computed["section2_type"]="spor"
			computed["section3_type"]="diffuse"
			computed["section4_type"]="stream"
		elif size in ["ultra","mega"]:
			computed["glowkey"]=7
			computed["section0_type"]="elec"
			computed["section1_type"]="psy"
			computed["section2_type"]="spor"
			computed["section3_type"]="diffuse"
			computed["section4_type"]="stream"

	computed["section0_flicker"]=0.2*flicker_mult
	computed["section1_flicker"]=0.14*flicker_mult
	computed["section2_flicker"]=0.3*flicker_mult
	computed["section3_flicker"]=0.4*flicker_mult
	computed["section4_flicker"]=0.7*flicker_mult

	flags = []
	if size != "lght" and params["name"].lower() != "flak" or params["name"][:4]=="beam":
		flags.append("Huge")
	if size != "lght" and size != "std" and params["name"].lower() != "flak" or params["name"][:4]=="beam":
		flags.append("Supercap")
	files[filename] += content.format(extraflags=" ".join("\"" + x + "\"" for x in flags), **computed) + "\n\n"

def run():
	for template in os.listdir("templates"):
		with open("templates/"+template) as f:
			entries=tuple(yaml.load_all(f))
			params=entries[0]
			templates=entries[1]
			for filename,content in templates.items():
				if filename not in files:
					files[filename]=""
					files[filename]="; Vanguards generated weapon table, do not edit\n\n#"+params["type"].title()+"\n\n"

				if ("{color}" in content or "{colorkey}" in content):
					if params["name"][:4]=="beam":
						colorset={k:v for k,v in colors.items() if k in ["yellow","green","blue","cyan","chartreuse","purple"]}
					else:
						colorset=colors
				else:
					colorset=[""]

				if "{size}" in content:
					if params["name"][:4]=="beam":
						sizeset=sizes
					else:
						sizeset=sizes[:-3]
				else:
					sizeset=[""]

				if "{speed}" in content:
					if params["name"][:4]=="beam":
						speedset=speeds
					else:
						speedset=[""]
				else:
					speedset=[""]

				for color in colorset:
					for size in sizeset:
						for speed in speedset:
							generate(params,color,size,speed,filename,content)

				files[filename]+="#End\n"

	os.makedirs("out/",exist_ok=True)
	for filename,contents in files.items():
		with open("out/"+filename,"w") as f:
			f.write(contents)

if __name__ == "__main__":
	run()
