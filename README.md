FSOscripts
==========

Assorted scripts for working with [Freespace Open](https://www.hard-light.net) files.

I typically do software development in a linux environment, so these are written for a linux-like system even though I do most freespace mod work on windows. Sorry if that's annoying. I use dual-boot or VMs with shared folders or WSL and then switch over to windows after I've run the scripts as needed.
