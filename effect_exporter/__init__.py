#!/usr/bin/env python3
import os,re,subprocess,tempfile,warnings

# requires http://www.fmwconcepts.com/imagemagick/dominantcolor/index.php

src_folder= "/path/to/fso/data/effects/"
dest_folder= os.path.dirname(__file__)+"/out/"

# name of color, hue out of 360, filename short name
colorset=[["red", 355, "r"], ["orange",35,"o"], ["yellow",55,"y"], ["chartreuse",90,"h"], ["green",120,"g"], ["seafoam",150,"s"], ["cyan",180,"c"], ["azure",205,"a"], ["blue",240,"b"], ["indigo",270,"i"], ["purple",290,"p"], ["magenta",320,"m"]]

# Syntax
# Each row is a specification of a file to export.
#
# If you just want to copy the file and keep the same name, the row is a string which is a regular expression matching
# the filename (must match full file name without suffix).
#
# Note that even for "copied" files, this tool will pass the image through imagemagick's DDS filters to ensure it uses
# the right compression type (DXT1) for vanilla and vanilla-alike image files (black alpha).
#
# If you want to rename the file in the destination, the row is a two-element array, with the first element being
# a regular expression matching the original filename, and the second being a template string for the final filename.
#
# If you are generating colors (see below) and only want to generate a single specific color, the array may contain a
# third element specifying the color you want to generate. This can be a string or a list of multiple strings.
#
# Additionally, if you want the color detection algorithm to prefer the secondary color instead of the primary, the
# third (or fourth) element can be set to True. Use this if color generation produces an unexpected color as a result.
#
# In regular expressions, be careful about being too greedy, for instance, if you use .*, you may accidentally grab the
# _0000 specifier that should be handled by {d} instead, causing double-iteration. Try using [^_]* instead.
#
# Template parameters:
#
# {d}		Handles the use of _0000 ... and similar for .dds animations in filenames so you don't have to copy each
# 			individually. Also grabs the .eff file. Both original and final can use {d}.
#
# {ani}     indicates this is an old vanilla engine .ani file
#
# {color}	The final filename can specify {color} to auto-generate different color variations of the source image.
#
# {customX}	The final filename can specify {custom1} to insert the first capturing group from the original filename's
#			regex, {custom2} for the second group, etc.
#

files=[
	## FS2 and FS1 MVPs and vanilla files ##

	# beams
	[r"AAABeamAA",r"beam-cloud-{color}"],
	[r"AAABeamAB{d}",r"beam-hazy-{color}{d}"],
	[r"AAABeamAB",r"beam-hazy-{color}"],
	[r"AAABeamAC",r"beam-spiky-{color}"],
	[r"AAABeamAD",r"beam-spor-{color}"],
	[r"BeamGlow3",r"beam-glow2-{color}"],
	[r"BeamGlow6{d}",r"beam-glow3-{color}{d}"],
	[r"BeamGlow6",r"beam-glow3-{color}"],
	[r"BeamGlow8",r"beam-glow-{color}"],
	[r"GreenBeam2Glow{d}",r"beam-glow4-{color}{d}"],
	[r"GreenBeam2Glow",r"beam-glow4-{color}"],
	[r"GreenBeamGlow{d}",r"beam-glow5-{color}{d}"],
	[r"GreenBeamGlow",r"beam-glow5-{color}"],
	[r"S_BeamGlow",r"beam-glow6-{color}"],
	[r"SBeamAGlow{d}",r"beam-glow7-{color}{d}"],
	[r"SBeamAGlow",r"beam-glow7-{color}"],
	[r"TBeamAGlow{d}",r"beam-glow8-{color}{d}"],
	[r"TBeamAGlow",r"beam-glow8-{color}"],
	[r"AAABeamAGlow{d}",r"beam-glow9-{color}{d}"],
	[r"AAABeamAGlow",r"beam-glow9-{color}"],
	[r"SAAACore",r"beam-diffuse-{color}"],
	[r"SBeamAA",r"beam-stable-{color}"],
	[r"SBeamAC{d}",r"beam-elec-{color}{d}"],
	[r"SBeamAC",r"beam-elec-{color}"],
	[r"SBeamAF{d}",r"beam-psy-{color}{d}"],
	[r"SBeamAF",r"beam-psy-{color}"],
	[r"SSL_D",r"beam-stream-{color}"],
	[r"TerBeam1Core",r"beam-ustab-{color}",True],
	[r"TerBeam2Core",r"beam-sstab-{color}"],
	[r"TerBeam2CoreHaze",r"beam-sstab-haze-{color}"],
	[r"TerBeam2Glow",r"beam-sstab-glow-{color}"],
	[r"TerBeam2GlowHaze",r"beam-sstab-ghaze-{color}"],
	[r"TerBeam4CoreHaze",r"beam-plasma-{color}"],
	[r"VasBeam2Core",r"beam-whoosh-{color}"],
	[r"VasBeam2Glow",r"beam-whoosh-glow-{color}"],
	[r"VasBeam2GlowHaze",r"beam-whoosh-ghaze-{color}"],
	#blobs
	[r"blobparticlegreen{d}",r"blob-part-{color}{d}"],
	[r"green_blob_trail",r"blob-trail-{color}"],
	[r"greenblob([^_]*)",r"blob-{custom1}-{color}"],
	[r"yellowblob(large|small)",r"blob-{custom1}-{color}"],
	[r"yellowblobmuzzle{d}",r"blob-muzz-{color}{d}"],
	[r"terranblob(\d*)",r"blob-ex-{custom1}-{color}"],
	[r"terranblobmuzzle{d}",r"blob-ex-muzz-{color}{d}"],
	[r"terranblobmuzzle2{d}",r"blob-ex-muzz2-{color}{d}",True],
	[r"terranblobtrail",r"blob-ex-trai-{color}"],
	[r"laserglow04",r"blob-glow-{color}"],
	[r"prometheus_AniBitmap_0011",r"blob-bolt2-{color}"],
	[r"prometheusr_AniBitmap_0011",r"blob-bolt-{color}"],
	#flak
	[r"flak_trail",r"flak-trail-{color}",True],
	[r"flakpart{d}",r"flak-part-{color}{d}"],
	[r"flakbright{d}",r"flak-brgh-{color}{d}"],
	[r"flakmuzzle{d}",r"flak-muzz-{color}{d}"],
	[r"flaksplode{d}",r"flak-expl-{color}{d}",True],
	[r"yarflak{d}",r"flak-yar-{color}{d}"],
	#missiles
	[r"missileglow05",r"miss-glow-{color}"],
	[r"missilespew05{d}",r"miss-spew-{color}{d}"],
	[r"missilethruster05",r"miss-thruster-{color}"],
	[r"missiletrail05",r"miss-trail-{color}"],
	#misc
	[r"Yellow_Glow",r"glow-{color}"],
	[r"Yellow_Glow_Flare",r"glow-flare-{color}"],
	[r"Thruster03-01",r"thruster1-back-{color}"],
	[r"Thruster03-02",r"thruster2-back-{color}"],
	[r"Thruster03-03",r"thruster3-back-{color}"],
	[r"Thruster03-01a",r"thruster4-back-{color}"],
	[r"Thruster03-02a",r"thruster5-back-{color}"],
	[r"Thruster03-03a",r"thruster6-back-{color}"],
	[r"ThrusterGlow01",r"thruster1-front-{color}"],
	[r"ThrusterGlow02",r"thruster2-front-{color}"],
	[r"ThrusterGlow03",r"thruster3-front-{color}"],
	[r"ThrusterGlow01a",r"thruster4-front-{color}"],
	[r"ThrusterGlow02a",r"thruster5-front-{color}"],
	[r"ThrusterGlow03a",r"thruster6-front-{color}"],
	[r"capflash{d}",r"capflash-{color}{d}"],
	# no colorization, just copy
	r"debris01{ani}",
	r"contrail",
	r"contrail01",
	r"transparent",
	r"nbackblue", # for some reason imagemagick does not like the nback.* ddses, just open them all and resave them in something like gimp (do not use gimp's overwrite menu item, set compression to dxt1)
	r"nbackcyan",
	r"nbackgreen",
	r"nbackpurp1",
	r"nbackpurp2",
	r"nbackred",
	r"nbackyellow",
	r"nbackorange",
	r"PoofGreen01",
	r"PoofGreen02",
	r"PoofRed01",
	r"PoofRed02",
	r"PoofPurp01",
	r"PoofPurp02",
	[r"Planet([A-Z])",r"Planet{custom1}"],
	[r"Neb(\d\d)",r"Neb{custom1}"],
	[r"DNeb(\d\d)",r"DNeb{custom1}"],
	[r"Nebul([A-Z]\d?\d)-(.*)",r"Nebul{custom1}-{custom2}"],
	[r"Sun(.*)",r"Sun{custom1}"],
	r"warpmap01{d}",
	r"warp_flare",
	r"flakburst{d}",
	r"capflash{d}",
	[r"terranblobglow(\d*)",r"blob-ex-glow{custom1}"],
	[r"exp([0-9]*){d}",r"exp{custom1}{d}"],
	[r"explode([0-9]*){d}",r"explode{custom1}{d}"],
	[r"expmissilehit([0-9]*){d}",r"expmissilehit{custom1}{d}"],
	r"flamewave{d}",
	r"fusiontrail{d}",
	[r"megafunk([^_]*){d}",r"megafunk{custom1}{d}"],
	[r"particleexp([0-9]*){d}",r"particleexp{custom1}{d}"],
	[r"particlesmoke([0-9]*){d}",r"particlesmoke{custom1}{d}"],
	r"rock_exp{d}",
	[r"S(_?)Beam([^_]*)",r"S{custom1}Beam{custom2}"],
	[r"S(_?)Beam([^_]*){d}",r"S{custom1}Beam{custom2}{d}"],
	r"SAAACore",
	r"ShieldHit03a{d}",
	[r"shiv([^_]*)",r"shiv{custom1}"],
	[r"shiv([^_]*){d}",r"shiv{custom1}{d}"],
	[r"shivan_([^_]*)",r"shivan_{custom1}"],
	[r"shivan([^_]*)_([^_]*)",r"shivan{custom1}_{custom2}"],
	[r"shiva([^_]*)_([^_]*)",r"shiva{custom1}_{custom2}"],
	r"Shockwave01{d}",
	[r"SSL_([^_]*)",r"SSL_{custom1}"],
	[r"SSL_([^_]*){d}",r"SSL_{custom1}{d}"],

	## EA ##

	[r"ehit-yellow{d}",r"ehit-y{d}"],
	[r"ehit-red{d}",r"ehit-r{d}"],
	[r"ehit-orange{d}",r"ehit-o{d}"],
	[r"ehit-pink{d}",r"ehit-p{d}"],
	[r"ehit-purple{d}",r"ehit-i{d}"],
	[r"ehit-turqoise{d}",r"ehit-c{d}"],
	[r"ehit-cyan{d}",r"ehit-a{d}"],
	[r"ehit-red{d}",r"ehit-{color}{d}",["green","chartreuse","blue","seafoam","magenta"]],

	## Custom Assets ##
	[r"flakflash{d}",r"flak-flash-{color}{d}",True], # derived from mvps, partially hueshifted
	[r"ShieldHit01a{d}",r"ShieldHit01a-{color}{d}"], # derived from mvps, sepiad and hueshifted
	[r"ShieldHit02a{d}",r"ShieldHit02a-{color}{d}"], # derived from mvps, sepiad and hueshifted
]

def t(pattern,dest_folder,src_folder,dest=False):
	for f in os.listdir(dest_folder if dest else src_folder):
		match=re.fullmatch(pattern,os.path.split(f)[1],flags=re.I)
		if match:
			return match
	return False

def a(pattern,dest_folder,src_folder,dest=False):
	a=[]
	for f in os.listdir(dest_folder if dest else src_folder):
		match=re.fullmatch(pattern,os.path.split(f)[1],flags=re.I)
		if match:
			a.append(match)
	return a

def run():
	global files
	global src_folder
	global dest_folder
	global colorset
	os.makedirs(dest_folder, exist_ok=True)
	for f in files:
		if type(f) is str:
			f=[f]
		orig_filename=f[0]
		final_filename=f[0]
		color_only=None
		secondary_color=False
		if len(f)>=2:
			orig_filename=f[0]
			final_filename=f[1]
		if len(f)>=3:
			if type(f[2]) is bool:
				secondary_color=f[2]
			else:
				if type(f[2]) is str:
					f[2]=[f[2]]
				color_only=f[2]
		if len(f)>=4:
			secondary_color=f[3]
		print()
		print("::",orig_filename,"=>",final_filename,"::")

		all_customs=[[]]
		if "{custom1}" in final_filename:
			all_customs=[]
			a_src=a(orig_filename.format(d="",ani="")+r"(?:\.dds|\.eff|\.ani)",dest_folder,src_folder)
			all_customs1=[list(custom.groups()) for custom in a_src]
			if len(all_customs1)==0:
				warnings.warn("Source file not found!")
			for index in range(0,len(all_customs1)):
				unique=True
				for custom_groups2 in all_customs:
					test=False
					for custom in range(0,len(all_customs1[index])):
						if all_customs1[index][custom]!=custom_groups2[custom]:
							test=True
							break
					if not test:
						unique=False
				if unique:
					if "{d}" in final_filename and ".eff" in a_src[index][0] or "{d}" not in final_filename and not re.search(r"(_\d\d\d\d)",a_src[index][0]) and not ".eff" in a_src[index][0]:
						all_customs.append([x.lower() for x in all_customs1[index]])

			matched=True
			index=1
			while matched:
				new_filename=re.sub(r"\([^?][^\)]*\)","{custom"+str(index)+"}",orig_filename,count=1) # using regexes to modify regexes... This is not 100% accurate, a capturing group that contains a literal ) in the filename or nested groups will not work, but it's unlikely we'll ever encounter those. I think the non-capturing group avoidance is a little bit overzealous too.
				matched=new_filename!=orig_filename
				orig_filename=new_filename
				index+=1

		for color,hue,colorshort in colorset:
			if color_only is None or color in color_only:
				for custom_groups in all_customs:
					custom_groups={"custom"+str(k+1):custom_groups[k] for k in range(0,len(custom_groups))}
					opsets=[]
					if "{d}" in orig_filename and "{d}" not in final_filename or "{d}" not in orig_filename and "{d}" in final_filename:
							raise Exception("Digits must be supplied in both filenames.")
					if "{d}" in orig_filename:
						opsets.append([
							(final_filename.format(color=colorshort, d=r"_\d\d\d\d", ani="", **custom_groups) + r"\.dds"),
							(orig_filename.format(d=r"(_\d\d\d\d)", ani="", **custom_groups) + r"\.dds"),
							r"(_\d\d\d\d)",
							".dds"
						])
						opsets.append([
							(final_filename.format(color=colorshort, d="", ani="", **custom_groups) + r"\.eff"),
							(orig_filename.format(d=r"", ani="", **custom_groups) + r"\.eff"),
							"",
							".eff"
						])
					elif "{ani}" in orig_filename:
						opsets.append([
							(final_filename.format(color=r"", d=r"", ani="", **custom_groups) + r"\.ani"),
							(orig_filename.format(d=r"", ani="", **custom_groups) + r"\.ani"),
							"",
							".ani"
						])
					else:
						opsets.append([
							(final_filename.format(color=colorshort, d=r"", ani="", **custom_groups) + r"\.dds"),
							(orig_filename.format(d=r"", ani="", **custom_groups) + r"\.dds"),
							"",
							".dds"
						])
					for opset in opsets:
						a_dest=a(opset[0],dest_folder,src_folder, dest=True)
						a_src=a(opset[1],dest_folder,src_folder)
						t_dest=t(opset[0],dest_folder,src_folder, dest=True)
						t_src=t(opset[1],dest_folder,src_folder)
						if not t_dest:
							# there is no destination file
							if t_src:
								# there is a source file
								sources={}
								if not opset[3]==".eff" and not opset[3]==".ani" and "{color}" in final_filename:
									print("generating",opset[0],"from",opset[1])
									sources={a[0]:"" for a in a_src}
									prev_color=None
									for source in sources.keys():
										# get dominant hue of file
										source_color=prev_color
										if prev_color is None or prev_color=="#FFFFFF":
											op="head"
											if secondary_color:
												op="tail"
											source_color=subprocess.run("dominantcolor -m 1 -n 2 -p all \""+src_folder+source+"\" | tail -n+3 | sed \"s/[^,]*,//\" | grep -v \"#FFFFFF\" | "+op+" -n1", shell=True, capture_output=True).stdout.decode('utf-8').strip()
										if source_color is None or source_color=="":
											source_color="#FFFFFF"
										prev_color=source_color
										source_hue=get_hue(source_color)
										# hue rotate to temp file location
										modulate_arg=int(((hue-source_hue)*100/180)+100)%200
										sources[source]=tempfile.mkstemp(".png")[1]
										subprocess.run("convert \"" + src_folder + source + "\" -modulate 100,100," + str(modulate_arg) + " " + sources[source], shell=True)
								else:
									print("copying",opset[0],"from",opset[1])
									sources={a[0]: src_folder + a[0] for a in a_src}
								for source,location in sources.items():
									# copy file to destination
									match=re.fullmatch(opset[1],source,flags=re.I)
									suffix=match.groups()[-1] if "{d}" in orig_filename and opset[2]!="" else ""
									outname=dest_folder + final_filename.format(color=colorshort, d=suffix, ani="", **custom_groups) + opset[3]
									if opset[3]==".dds":
										tempname=tempfile.mkstemp(".dds")[1]
										subprocess.run("convert \"" + location + "\" -define dds:compression=dxt1 -define dds:mipmaps=0 " + tempname, shell=True)
									else:
										tempname=location
									subprocess.run("cp \"" + tempname +"\" \"" + outname + "\"", shell=True)
									if tempname[:5]=="/tmp/":
										os.unlink(tempname)
									if location[:5]=="/tmp/":
										os.unlink(location)
							else:
								warnings.warn("Source file not found!")
						else:
							print(opset[0],"exists")
				if "{color}" not in final_filename:
					break

if __name__ == "__main__":
	run()
