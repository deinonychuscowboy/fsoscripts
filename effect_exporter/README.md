Effect Exporter
===============

This script exports items from the freespace data/effects/ folder for creating a set of new mod effects from base game
or other mods' sources. It is capable of:
- automatically handling the multiple files needed for an animation with one copy rule
- using regular expressions and templates to grab multiple files with one rule
- automatically generating multiple color variants from a single source file via hue rotation

The rules I'm using for my mod are included as an example. Sorry for renaming everything, the lack of a consistent
naming scheme in freespace's OG source files annoys me.

Prerequisites
-------------

http://www.fmwconcepts.com/imagemagick/dominantcolor/index.php
